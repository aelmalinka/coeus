/*	Copyright 2014 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Source.hh"

#include <boost/log/attributes/constant.hpp>
#include <boost/log/utility/manipulators/add_value.hpp>

using namespace Coeus::Log;
using namespace std;
using namespace boost::log;

Source::~Source() = default;

Source::Source()
	: severity_channel_logger_mt(keywords::channel = "Application", keywords::severity = Severity::Info)
{}

Source::Source(const string &chan)
	: severity_channel_logger_mt(keywords::channel = chan, keywords::severity = Severity::Info)
{}

Source::Source(const Severity &sev)
	: severity_channel_logger_mt(keywords::channel = "Application", keywords::severity = sev)
{}

Source::Source(const string &chan, const Severity &sev)
	: severity_channel_logger_mt(keywords::channel = chan, keywords::severity = sev)
{}

Source &Source::operator << (const exception &e)
{
	using ::Coeus::Function;
	using ::Coeus::File;
	using ::Coeus::Line;
	using ::boost::get_error_info;

	auto s = get_error_info<SeverityInfo>(e);
	Severity sev = (s) ? *s : Severity::Fatal;

	record rec = this->open_record(keywords::severity = sev);
	if(rec)
	{
		record_ostream strm(rec);

		auto w = get_error_info<Function>(e);
		auto f = get_error_info<File>(e);
		auto l = get_error_info<Line>(e);

		if(w) strm << add_value("Function", *w);
		if(f) strm << add_value("File", *f);
		if(l) strm << add_value("Line", *l);

		strm << e.what() << flush;

		this->push_record(move(rec));
	}

	return *this;
}
