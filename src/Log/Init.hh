/*	Copyright 2014 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined COEUS_LOG_INIT_INC
#	define COEUS_LOG_INIT_INC

#	include <boost/log/common.hpp>

	namespace Coeus
	{
		namespace Log
		{
			namespace detail
			{
				struct Initializer
				{
					Initializer();
				};

				static const Initializer Init;
			}
		}
	}

#endif
