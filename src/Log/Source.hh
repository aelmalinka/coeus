/*	Copyright 2014 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined COEUS_LOG_SOURCE_INC
#	define COEUS_LOG_SOURCE_INC

#	include "../Log.hh"

#	include <boost/log/sources/severity_channel_logger.hpp>

	namespace Coeus
	{
		namespace Log
		{
			class Source :
				public boost::log::sources::severity_channel_logger_mt<Severity, std::string>
			{
				public:
					Source();
					explicit Source(const std::string &);
					explicit Source(const Severity &);
					Source(const std::string &, const Severity &);
					virtual ~Source();
					Source &operator << (const std::exception &);
			};
		}
	}

#endif
