/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined COEUS_APPLICATION_INC
#	define COEUS_APPLICATION_INC

#	include "App/Boost.hh"

	namespace Coeus
	{
		using App::options_description;
		using App::value;

		extern struct nodefault_t {} nodefault;

		class Application :
			public App::Boost
		{
			public:
				Application();
				Application(const nodefault_t &);
				Application(const int, const char *[]);
				Application(const int, const char *[], const nodefault_t &);
				virtual ~Application();
				virtual void operator () () override;
			protected:
				virtual std::filesystem::path configfile() override;
			private:
				void setupParams();
			private:
				App::options_description _default;
				std::string _config;
		};
	}


#endif
