/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined COEUS_IMPORT_INC
#	define COEUS_IMPORT_INC

#	include <filesystem>
#	include <list>
#	include <unordered_map>

#	include "Exception.hh"

//	2020-01-03 AMR TODO: boost::dll seems to need work (avoid or fix?)
#	define BOOST_DLL_USE_STD_FS
#	include <boost/dll/shared_library.hpp>

	namespace Coeus
	{
		COEUS_EXCEPTION(ImportException, "Import Exception", Coeus::Exception);

		// 2020-01-03 AMR TODO: support constructor params
		template<typename T>
		class Import
		{
			public:
				Import();
				explicit Import(const std::string &);
				template<typename U>
				explicit Import(const std::shared_ptr<U> &);
				template<typename U>
				Import(const Import<U> &);
				template<typename U>
				Import(Import<U> &&);
				bool is_loaded() const;
				bool is_created() const;
				template<typename U>
				Import<T> &operator = (const Import<U> &);
				template<typename U>
				Import<T> &operator = (const std::shared_ptr<U> &);
				template<typename U>
				Import<T> &operator = (Import<U> &&);
				template<typename U>
				Import<T> &operator = (std::shared_ptr<U> &&);
				operator bool () const;
				T &operator * ();
				T *operator ->();
				const T &operator * () const;
				const T *operator ->() const;
				static void addSearchPath(const std::filesystem::path &);
			private:
				void load(const std::filesystem::path &);
				std::shared_ptr<T> create();
			private:
				boost::dll::shared_library _library;
				std::shared_ptr<T> _obj;
				static std::list<std::filesystem::path> _search;
				static constexpr const char *_create = "coeus_create";
				static constexpr const char *_delete = "coeus_delete";
		};
	}

#	include "Import.impl.hh"

#	define COEUS_MODULE(TYPE) extern "C" {\
			TYPE *coeus_create() {\
				return new TYPE; \
			} \
			void coeus_delete(void *p) {\
				if(!p) return;\
				delete static_cast<TYPE *>(p); \
			} \
		}

#endif
