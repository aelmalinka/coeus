/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined COEUS_SHARED_DATA_IMPL
#	define COEUS_SHARED_DATA_IMPL

#	include <utility>

	namespace Coeus
	{
		namespace detail {
			template<typename C>
			struct make_c {};

			template<typename T>
			struct make_c<std::unique_ptr<T>> {
				template<typename ...Args>
				std::unique_ptr<T> operator () (Args && ...args) {
					return std::make_unique<T>(std::forward<Args>(args)...);
				}
			};

			template<typename T>
			struct make_c<std::shared_ptr<T>> {
				template<typename ...Args>
				std::shared_ptr<T> operator () (Args && ...args) {
					return std::make_shared<T>(std::forward<Args>(args)...);
				}
			};

			template<typename T>
			struct make_c<T *> {
				template<typename ...Args>
				T *operator () (Args && ...args) {
					return new T(std::forward<Args>(args)...);
				}
			};

			template<typename C>
			void erase_c(C &c) {
				static_assert(!std::is_pointer<C>::value, "dumb pointers will leak here");
				c = C();
			}

			template<typename T>
			void erase_c(T *c) {
				delete c;
			}
		}

		template<typename T, typename C>
		C SharedData<T, C>::_data;

		template<typename T, typename C>
		std::size_t SharedData<T, C>::_count = 0;

		template<typename T, typename C>
		template<typename ...Args>
		SharedData<T, C>::SharedData(Args && ...args) {
			if(_count++ == 0)
				_data = detail::make_c<C>()(std::forward<Args>(args)...);
		}

		template<typename T, typename C>
		SharedData<T, C>::~SharedData() {
			if(--_count == 0)
				detail::erase_c(_data);
		}

		template<typename T, typename C>
		SharedData<T, C>::SharedData(SharedData<T, C> const &) {
			++_count;
		}

		template<typename T, typename C>
		SharedData<T, C>::SharedData(SharedData<T, C> &&) {
			++_count;
		}

		template<typename T, typename C>
		auto SharedData<T, C>::operator = (SharedData<T, C> const &) -> decltype(*this) & {
			++_count;
			return *this;
		}

		template<typename T, typename C>
		auto SharedData<T, C>::operator = (SharedData<T, C> &&) -> decltype(*this) & {
			++_count;
			return *this;
		}

		template<typename T, typename C>
		auto SharedData<T, C>::shared() const -> decltype(_data) const & {
			return _data;
		}
	}

#endif
