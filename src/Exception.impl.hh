/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined COEUS_EXCEPTION_IMPL
#	define COEUS_EXCEPTION_IMPL

#	include "Exception.hh"

	namespace Coeus
	{
		template<typename ErrorInfo>
		const typename ErrorInfo::value_type *ExceptionBase::get() const
		{
			return boost::get_error_info<ErrorInfo>(*this);
		}

		template<typename ErrorInfo>
		bool ExceptionBase::has() const
		{
			return boost::get_error_info<ErrorInfo>(*this) != nullptr;
		}

		template<typename E>
		typename detail::WithErrorInfo<E>::type enable_trace(const E &e)
		{
			auto ret(::boost::enable_error_info(e));

			AttachTrace(ret);

			return ret;
		}
	}

	template<typename charT>
	std::basic_ostream<charT> &operator << (std::basic_ostream<charT> &os, const std::exception &e)
	{
		using std::endl;
		using boost::get_error_info;
		using namespace Coeus;

		os << e.what();

		if(get_error_info<SystemError>(e) != nullptr) {
			os << " " << *get_error_info<SystemError>(e);
		}

		if(get_error_info<ErrorCode>(e) != nullptr) {
			os << " (" << *get_error_info<ErrorCode>(e) << ")";
		}

		if(get_error_info<Stack>(e) != nullptr) {
			os << endl << *get_error_info<Stack>(e);
		}

		// 2019-09-03 AMR TODO: this won't be defined with config.h only def
#		if defined DEBUG
			using std::basic_string;
			using boost::diagnostic_information;

			basic_string<charT> s = diagnostic_information(e);
			s = s.substr(0, s.size() - 1);
			os << endl << s;
#		endif

		return os;
	}

#endif
