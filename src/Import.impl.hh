/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined COEUS_IMPORT_IMPL
#	define COEUS_IMPORT_IMPL

#	include <boost/dll/import.hpp>

	namespace Coeus
	{
		template<typename T>
		std::list<std::filesystem::path> Import<T>::_search = {
			std::filesystem::path("."),
		};

		template<typename T>
		void Import<T>::addSearchPath(const std::filesystem::path &p) {
			_search.emplace_back(p);
		}

		template<typename T>
		Import<T>::Import() :
			_library(),
			_obj()
		{}

		template<typename T>
		template<typename U>
		Import<T>::Import(const Import<U> &o)  :
			_library(o._library),
			_obj(o._obj)
		{}

		template<typename T>
		template<typename U>
		Import<T>::Import(Import<U> &&o) :
			_library(std::move(o._library)),
			_obj(std::move(o._obj))
		{}

		template<typename T>
		Import<T>::Import(const std::string &name) :
			_library(),
			_obj()
		{
			using namespace std;
			using namespace std::filesystem;
			using namespace boost::dll;

			for(const auto &p : _search) {
				if(exists(p)) {
					load(p / name);

					if(is_loaded()) {
						_obj = create();
						break;
					}
				}
			}
		}

		template<typename T>
		template<typename U>
		Import<T>::Import(const std::shared_ptr<U> &obj) :
			_library(),
			_obj(obj)
		{}

		template<typename T>
		void Import<T>::load(const std::filesystem::path &path) {
			// 2020-01-04 AMR TODO: handle not not exists
			boost::dll::fs::error_code ec;
			_library.load(path, ec, boost::dll::load_mode::append_decorations);
		}

		template<typename T>
		std::shared_ptr<T> Import<T>::create() {
			using namespace std;
			using namespace boost::dll;

			auto t_create = import_symbol<T *()>(_library, _create);
			auto t_delete = import_symbol<void (void *)>(_library, _delete);

			T *ptr = nullptr;

			if(_library.has(_create) && _library.has(_delete)) {
				try {
					ptr = t_create();

					return shared_ptr<T>(ptr, t_delete);
				} catch(...) {
					if(ptr != nullptr)
						t_delete(ptr);

					throw;
				}
			}

			COEUS_THROW(ImportException("Import aliases not found"));
		}

		template<typename T>
		bool Import<T>::is_loaded() const {
			return _library.is_loaded();
		}

		template<typename T>
		bool Import<T>::is_created() const {
			return static_cast<bool>(_obj);
		}

		template<typename T>
		template<typename U>
		Import<T> &Import<T>::operator = (const Import<U> &o) {
			_library = o._libraryu;
			_obj = o._obj;

			return *this;
		}

		template<typename T>
		template<typename U>
		Import<T> &Import<T>::operator = (const std::shared_ptr<U> &o) {
			_obj = o;

			return *this;
		}

		template<typename T>
		template<typename U>
		Import<T> &Import<T>::operator = (Import<U> &&o) {
			std::swap(_library, o._library);
			std::swap(_obj, o._obj);

			return *this;
		}

		template<typename T>
		template<typename U>
		Import<T> &Import<T>::operator = (std::shared_ptr<U> &&o) {
			std::swap(_obj, o);

			return *this;
		}

		template<typename T>
		Import<T>::operator bool () const {
			return is_created();
		}

		template<typename T>
		T &Import<T>::operator * () {
			return *_obj;
		}

		template<typename T>
		T *Import<T>::operator -> () {
			return _obj.get();
		}

		template<typename T>
		const T &Import<T>::operator * () const {
			return *_obj;
		}

		template<typename T>
		const T *Import<T>::operator -> () const {
			return _obj.get();
		}
	}

#endif
