/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Exception.hh"

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

using namespace Coeus;
using namespace std;

ExceptionBase::ExceptionBase() = default;

ExceptionBase::ExceptionBase(const std::string &Type) :
	boost::exception(), m_type(Type), m_desc(), m_what(Type)
{}

ExceptionBase::ExceptionBase(const std::string &Type, const std::string &Desc) :
	boost::exception(), m_type(Type), m_desc(Desc), m_what(((Desc != "") ? Type + ": " + Desc : Type))
{}

ExceptionBase::~ExceptionBase() noexcept = default;

const char *ExceptionBase::what() const noexcept
{
	return m_what.c_str();
}

Exception::Exception(const std::string &Type)
	: std::exception(), ExceptionBase(Type)
{}

Exception::Exception(const std::string &Type, const std::string &Desc)
	: std::exception(), ExceptionBase(Type, Desc)
{}

Exception::~Exception() noexcept = default;

const char *Exception::what() const noexcept
{
	return ExceptionBase::what();
}

namespace Coeus {
	void AttachTrace(boost::exception &e)
	{
#		ifdef USE_STACKTRACE
			e << Stack(::boost::stacktrace::stacktrace());
#		else
			(void)e;
#		endif
	}
}
