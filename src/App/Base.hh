/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined COEUS_APP_BASE_INC
#	define COEUS_APP_BASE_INC

#	include "../Log.hh"

	namespace Coeus
	{
		namespace App
		{
			class Base
			{
				public:
					Base();
					Base(const int, const char *[]);
					Base(const Base &) = delete;
					virtual ~Base();
					virtual void operator () () = 0;
				protected:
					const int &ArgC();
					const char **ArgV();
				protected:
					Log::Source log;
				private:
					int _argc;
					const char **_argv;
			};
		}
	}

#endif
