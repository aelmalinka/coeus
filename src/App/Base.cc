/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Base.hh"

using namespace Coeus::App;
using namespace std;

static const char *DEFAULT[] = {
	"UNKNOWN",
};

Base::Base() :
	log("Application"),
	_argc(1),
	_argv(DEFAULT)
{}

Base::Base(const int argc, const char *argv[]) :
	log("Application"),
	_argc(argc),
	_argv(argv)
{}

Base::~Base() = default;

const int &Base::ArgC()
{
	return _argc;
}

const char **Base::ArgV()
{
	return _argv;
}
