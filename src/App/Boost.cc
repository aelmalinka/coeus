/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Boost.hh"
#include <functional>

using namespace Coeus::App;
using namespace std;
using namespace std::filesystem;

using boost::program_options::parse_command_line;
using boost::program_options::parse_config_file;
using boost::program_options::parse_environment;

Boost::Boost() :
	Base(),
	_command_line(),
	_config_file(),
	_environment(),
	_vm()
{}

Boost::Boost(const int argc, const char *argv[]) :
	Base(argc, argv),
	_command_line(),
	_config_file(),
	_environment(),
	_vm()
{}

Boost::~Boost()
{}

// 2019-11-14 AMR TODO: is this multiple notify going to be a problem?
void Boost::operator () ()
{
	store(parse_command_line(ArgC(), ArgV(), _command_line), _vm);
	notify(_vm);
	store(parse_environment(_environment, bind(&Boost::environment_map, this, placeholders::_1)), _vm);
	notify(_vm);
	auto p = configfile();
	COEUS_LOG(log, Severity::Debug) << "config file: " << p.string();
	if(exists(p))
		store(parse_config_file(p.string().c_str(), _config_file), _vm);
	notify(_vm);
}

string Boost::environment_map(const string &)
{
	return ""s;	// 2019-10-20 AMR TODO: should there be some expected mappings?
}

path Boost::configfile()
{
	return path();
}

variables_map &Boost::vm()
{
	return _vm;
}

options_description &Boost::command_line()
{
	return _command_line;
}

options_description &Boost::config_file()
{
	return _config_file;
}

options_description &Boost::environment()
{
	return _environment;
}

const variables_map &Boost::vm() const
{
	return _vm;
}
