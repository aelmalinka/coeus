/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined COEUS_BOOST_APP_INC
#	define COEUS_BOOST_APP_INC

#	include "Base.hh"
#	include <boost/program_options.hpp>
#	include <filesystem>

	namespace Coeus
	{
		namespace App
		{
			using boost::program_options::variables_map;
			using boost::program_options::options_description;
			using boost::program_options::positional_options_description;
			using boost::program_options::value;
			using boost::program_options::store;

			class Boost :
				public Base
			{
				public:
					Boost();
					Boost(const int, const char *[]);
					virtual ~Boost();
					virtual void operator () () override;
				protected:
					virtual std::string environment_map(const std::string &);
					virtual std::filesystem::path configfile();
					variables_map &vm();
					options_description &command_line();
					options_description &config_file();
					options_description &environment();
					const variables_map &vm() const;
				private:
					options_description _command_line;
					options_description _config_file;
					options_description _environment;
					variables_map _vm;
			};
		}
	}

#endif
