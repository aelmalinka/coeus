/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined COEUS_EVENT_INC
#	define COEUS_EVENT_INC

#	include <typeinfo>
#	include <typeindex>

	namespace Coeus
	{
		class Event {
			public:
				using id_type = decltype(std::type_index(typeid(int)));
			public:
				Event();
				virtual ~Event();
				auto Id() const -> id_type;
		};
	}

#endif
