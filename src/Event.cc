/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Event.hh"

using namespace Coeus;
using namespace std;

Event::Event() = default;
Event::~Event() = default;

auto Event::Id() const -> id_type {
	return type_index(typeid(*this));
}
