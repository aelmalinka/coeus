/*	Copyright 2010 Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined COEUS_EXCEPTION_INC
#	define COEUS_EXCEPTION_INC

#	include <boost/exception/exception.hpp>
#	include <boost/exception/enable_error_info.hpp>
#	include <boost/exception/get_error_info.hpp>
#	include <boost/exception/diagnostic_information.hpp>
#	include <boost/throw_exception.hpp>
#	include <boost/stacktrace.hpp>

#	include <stdexcept>
#	include <iostream>

	namespace Coeus
	{
		struct ErrorInfoNotFound;

		struct ExceptionBase :
			boost::exception
		{
			public:
				const char *what() const noexcept;
				~ExceptionBase() noexcept;
				// 2019-09-02 AMR NOTE: return pointer to avoid potential nastiness
				template<typename ErrorInfo>
				const typename ErrorInfo::value_type *get() const;
				template<typename ErrorInfo>
				bool has() const;
			protected:
				ExceptionBase();
				ExceptionBase(const std::string &Type);
				ExceptionBase(const std::string &Type, const std::string &Desc);
			private:
				std::string m_type;
				std::string m_desc;
				std::string m_what;
		};

		struct Exception :
			std::exception,
			ExceptionBase
		{
			virtual ~Exception() noexcept;
			virtual const char *what() const noexcept;
			protected:
				Exception(const std::string &Type);
				Exception(const std::string &Type, const std::string &Desc);
		};

#		define COEUS_EXCEPTION(NAME, DESC, BASE) struct NAME : BASE {\
			NAME() : BASE(DESC, "") {}\
			NAME(const std::string &What) : BASE(DESC, What) {}\
			NAME(const std::string &Desc, const std::string &What) : BASE(Desc, What) {} \
			virtual ~NAME() noexcept; }; \
			inline NAME::~NAME() noexcept = default

#		define COEUS_ERROR_INFO(NAME, TYPE)\
			typedef boost::error_info<struct tag_##NAME, TYPE> NAME

		COEUS_EXCEPTION(GeneralFailure, "General Failure", Exception);

		using Function = ::boost::throw_function;
		using File = ::boost::throw_file;
		using Line = ::boost::throw_line;

		COEUS_ERROR_INFO(SystemError, std::string);
		COEUS_ERROR_INFO(ErrorCode, std::error_code);
		COEUS_ERROR_INFO(Stack, boost::stacktrace::stacktrace);

		namespace detail
		{
			template<typename E>
			using WithErrorInfo = ::boost::exception_detail::enable_error_info_return_type<E>;
		}

		void AttachTrace(boost::exception &);

		template<typename E>
		typename detail::WithErrorInfo<E>::type enable_trace(const E &);

#		define COEUS_ATTACH_TRACE(x)\
			(::Coeus::enable_trace(x) <<\
			::Coeus::Function(BOOST_CURRENT_FUNCTION) <<\
			::Coeus::File(__FILE__) <<\
			::Coeus::Line((int)__LINE__))

#		define COEUS_THROW(x)\
			::boost::throw_exception(COEUS_ATTACH_TRACE(x))
	}

	template<typename charT>
	std::basic_ostream<charT> &operator << (std::basic_ostream<charT> &, const std::exception &);

#	include "Exception.impl.hh"

#endif
