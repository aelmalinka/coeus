/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Application.hh"

#include <cstdlib>
#include <iostream>

using namespace Coeus;
using namespace Coeus::App;
using namespace std;

namespace Coeus {
	nodefault_t nodefault;
}

Application::Application() :
	Application(nodefault)
{
	setupParams();
}

Application::Application(const nodefault_t &) :
	Boost()
{}

Application::Application(const int argc, const char *argv[]) :
	Application(argc, argv, nodefault)
{
	setupParams();
}

Application::Application(const int argc, const char *argv[], const nodefault_t &) :
	Boost(argc, argv)
{}

Application::~Application()
{}

void Application::operator () ()
{
	Boost::operator () ();

	if(vm().count("help") != 0) {
		cout << command_line() << endl;
		exit(64);	// 2019-11-20 AMR TODO: this could be better
	}
}

filesystem::path Application::configfile()
{
	return filesystem::path(_config);
}

void Application::setupParams()
{
	// 2019-11-20 AMR TODO: don't help in config/environment
	_default.add_options()
		("help,h", "Show this menu")
		("config-file,c", value<string>(&_config), "Where the config file is located")
	;

	command_line().add(_default);
	config_file().add(_default);
	environment().add(_default);
}
