/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined COEUS_SHARED_DATA_INC
#	define COEUS_SHARED_DATA_INC

#	include <memory>

	namespace Coeus
	{
		template<typename T, typename C = std::unique_ptr<T>>
		class SharedData {
			private:
				static C _data;
				static std::size_t _count;
			protected:
				template<typename ...Args>
				explicit SharedData(Args && ...);
				SharedData(SharedData<T, C> const &);
				SharedData(SharedData<T, C> &&);
				virtual ~SharedData();
				auto operator = (SharedData<T, C> const &) -> decltype(*this) &;
				auto operator = (SharedData<T, C> &&) -> decltype(*this) &;
				auto shared() const -> decltype(_data) const &;
		};
	}

#	include "SharedData.impl.hh"

#endif
