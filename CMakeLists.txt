# Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
# Distributed under the terms of the GNU Affero General Public License v3

cmake_minimum_required (VERSION 3.15)

project (Coeus VERSION 0.0.2)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")

include(Base)
include(Debug)
include(GTest)

option(USE_STACKTRACE "Stacktrace" "${DEBUG}")

list(APPEND BOOST_NEEDED_LIBS exception log program_options)

set(Boost_USE_STATIC_LIBS OFF)
set(Boost_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_RUNTIME OFF)
set(Boost_VERSION 1.78)
find_package(Boost ${Boost_VERSION} REQUIRED COMPONENTS ${BOOST_NEEDED_LIBS})

include_directories(${Boost_INCLUDE_DIRS})
link_directories(${Boost_LIBRARY_DIRS})

configure_file (
	"${PROJECT_SOURCE_DIR}/config.h.in"
	"${PROJECT_BINARY_DIR}/config.h"
)
include_directories("${PROJECT_BINARY_DIR}")
add_definitions(-DHAVE_CONFIG_H)

set(COEUS_HEADERS
	src/App/Base.hh
	src/App/Boost.hh
	src/Log/Init.hh
	src/Log/Severity.hh
	src/Log/Source.hh
	src/Application.hh
	src/Event.hh
	src/Exception.hh src/Exception.impl.hh
	src/Import.hh src/Import.impl.hh
	src/Log.hh
	src/SharedData.hh src/SharedData.impl.hh
)
set(COEUS_SOURCES
	src/App/Base.cc
	src/App/Boost.cc
	src/Log/Init.cc
	src/Log/Severity.cc
	src/Log/Source.cc
	src/Application.cc
	src/Event.cc
	src/Exception.cc
)
add_library(Coeus SHARED
	${COEUS_HEADERS} ${COEUS_SOURCES}
)
target_link_libraries(Coeus
	PUBLIC
		${Boost_LIBRARIES}
		${CMAKE_DL_LIBS}
)
set_target_properties(Coeus PROPERTIES
	VERSION ${PROJECT_VERSION}
	SOVERSION ${PROJECT_VERSION}
)

list(APPEND TEST_LIBS Coeus)
add_subdirectory(test)

list(APPEND INSTALL_LIBS Coeus)
include(Install)
