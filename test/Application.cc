/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Application.hh"
#include <vector>

#define TEST_BEGIN try {
#define TEST_END } catch(exception &e) { FAIL() << e.what() << endl; }

using namespace testing;
using namespace std;

namespace {
	using Coeus::nodefault_t;
	using Coeus::nodefault;

	class Application :
		public ::Coeus::Application
	{
		public:
			Application();
			Application(const int, const char *[]);
			Application(const int, const char *[], const nodefault_t &);
			void operator () ();
			operator bool () const;
			string ConfigFile();
			const int &ArgC();
			const char **ArgV();
		private:
			bool _ran;
	};

	TEST(ApplicationTests, NoParams) {
		TEST_BEGIN
		Application app;

		EXPECT_FALSE(app);
		app();
		EXPECT_TRUE(app);
		EXPECT_EQ(app.ArgC(), 1);
		EXPECT_NE(strlen(app.ArgV()[0]), 0);
		TEST_END
	}

	TEST(ApplicationTests, NameOnly) {
		TEST_BEGIN
		vector<const char *> arg = {
			"PROGRAM NAME",
		};
		Application app(arg.size(), arg.data());

		EXPECT_FALSE(app);
		app();
		EXPECT_TRUE(app);
		EXPECT_EQ(app.ArgC(), 1);
		EXPECT_EQ(app.ArgV()[0], "PROGRAM NAME"s);
		TEST_END
	}

	TEST(ApplicationTests, ConfigShort) {
		TEST_BEGIN
		const string file = "A File";
		vector<const char *> arg = {
			"PROGRAM NAME",
			"-c", file.c_str(),
		};
		Application app(arg.size(), arg.data());

		EXPECT_FALSE(app);
		app();
		EXPECT_TRUE(app);
		EXPECT_EQ(app.ConfigFile(), file);
		TEST_END
	}

	TEST(ApplicationTests, ConfigLong) {
		TEST_BEGIN
		const string file = "A File";
		vector<const char *> arg = {
			"PROGRAM NAME",
			"--config", file.c_str(),
		};
		Application app(arg.size(), arg.data());

		EXPECT_FALSE(app);
		app();
		EXPECT_TRUE(app);
		EXPECT_EQ(app.ConfigFile(), file);
		TEST_END
	}
	// 2019-11-09 AMR TODO: logging configuration
	// 2019-11-14 AMR TODO: don't require boost in this test
	TEST(ApplicationTests, NoDefault) {
		TEST_BEGIN
		const string file = "a file";
		vector<const char *> arg = {
			"PROGRAM NAME",
			"-c", file.c_str(),
		};
		Application app(arg.size(), arg.data(), nodefault);

		EXPECT_FALSE(app);
		EXPECT_THROW(app(), ::boost::program_options::unknown_option);
		EXPECT_FALSE(app);
		EXPECT_NE(app.ConfigFile(), file);
		TEST_END
	}

	TEST(ApplicationTests, HelpMenu) {
		TEST_BEGIN
		const string file = "a file";
		vector<const char *> arg = {
			"PROGRAM NAME",
			"--help"
		};
		Application app(arg.size(), arg.data());

		EXPECT_DEATH_IF_SUPPORTED(app(), "");
		TEST_END
	}

	void Application::operator () ()
	{
		Coeus::Application::operator () ();

		_ran = true;
	}

	string Application::ConfigFile()
	{
		return configfile().string();
	}

	Application::Application() :
		::Coeus::Application(),
		_ran(false)
	{}

	Application::Application(const int argc, const char *argv[]) :
		::Coeus::Application(argc, argv),
		_ran(false)
	{}

	Application::Application(const int argc, const char *argv[], const nodefault_t &nd) :
		::Coeus::Application(argc, argv, nd),
		_ran(false)
	{}

	Application::operator bool () const
	{
		return _ran;
	}

	const int &Application::ArgC()
	{
		return ::Coeus::Application::ArgC();
	}

	const char **Application::ArgV()
	{
		return ::Coeus::Application::ArgV();
	}
}
