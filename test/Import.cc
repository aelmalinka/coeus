/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Import.hh"
#include "Module.hh"

using namespace std;
using namespace testing;
using namespace Coeus;
using namespace Coeus::Tests;

namespace {
	TEST(ImportTests, Default) {
		Import<Module> m;
		Import<Module> o(m);

		EXPECT_FALSE(m);
		EXPECT_FALSE(o);

		Import<Module> t(move(m));

		EXPECT_FALSE(t);

		Import<Module> a = t;
		Import<Module> b = move(o);

		EXPECT_FALSE(a);
		EXPECT_FALSE(b);
	}

	TEST(ImportTests, Import) {
		Import<Module> m("module");

		ASSERT_TRUE(m);

		EXPECT_FALSE(m->isThing());
		EXPECT_FALSE(m->isOther());

		m->doThing();

		EXPECT_TRUE(m->isThing());
		EXPECT_FALSE(m->isOther());

		m->doOther();

		EXPECT_TRUE(m->isThing());
		EXPECT_TRUE(m->isOther());
	}

	TEST(ImportTests, Copy) {
		Import<Module> m("module");

		m->doThing();
		m->doOther();

		Import<Module> o(m);

		ASSERT_TRUE(o);
		EXPECT_TRUE(o->isThing());
		EXPECT_TRUE(o->isOther());

		Import<Module> a = o;

		ASSERT_TRUE(a);
		EXPECT_TRUE(a->isThing());
		EXPECT_TRUE(a->isOther());
	}

	TEST(ImportTests, Move) {
		Import<Module> m("module");

		m->doThing();
		m->doOther();

		Import<Module> o(move(m));

		ASSERT_TRUE(o);
		EXPECT_TRUE(o->isThing());
		EXPECT_TRUE(o->isOther());

		EXPECT_FALSE(m);

		Import<Module> a = move(o);

		ASSERT_TRUE(a);
		EXPECT_TRUE(a->isThing());
		EXPECT_TRUE(a->isOther());

		EXPECT_FALSE(o);
		EXPECT_FALSE(m);
	}

	TEST(ImportTests, NoFile) {
		Import<Module> m("ThisShouldNotBeAFileOnThisSystem");

		EXPECT_FALSE(m);
	}

	// 2020-01-20 AMR TODO: DRY?
	class ModuleImpl :
		public Module
	{
		public:
			ModuleImpl() :
				_thing(false),
				_other(false)
			{}
			void doThing() {
				_thing = true;
			}
			void doOther() {
				_other = true;
			}
			bool isThing() const {
				return _thing;
			}
			bool isOther() const {
				return _other;
			}
		private:
			bool _thing;
			bool _other;
	};

	TEST(ImportTests, SuppliedT) {
		auto o = make_shared<ModuleImpl>();
		auto m = Import<Module>(o);

		ASSERT_TRUE(m);

		EXPECT_FALSE(m->isThing());
		EXPECT_FALSE(m->isOther());

		m->doThing();

		EXPECT_TRUE(m->isThing());
		EXPECT_FALSE(m->isOther());

		m->doOther();

		EXPECT_TRUE(m->isThing());
		EXPECT_TRUE(m->isOther());
	}
}
