/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#if !defined COEUS_TEST_MODULE_INC
#	define COEUS_TEST_MODULE_INC

#	include "../src/Import.hh"

	namespace Coeus
	{
		namespace Tests
		{
			class Module {
				public:
					virtual ~Module() = default;
					virtual void doThing() = 0;
					virtual void doOther() = 0;
					virtual bool isThing() const = 0;
					virtual bool isOther() const = 0;
			};
		}
	}

#endif
