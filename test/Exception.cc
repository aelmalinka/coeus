/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Exception.hh"
#include <sstream>

#ifdef HAVE_CONFIG_H
#	include "config.h"
#endif

using namespace testing;
using namespace std;

namespace {
	COEUS_EXCEPTION(Exception, "Test Exception", ::Coeus::Exception);
	COEUS_ERROR_INFO(TestInt, int);
	COEUS_ERROR_INFO(TestString, string);

	TEST(ExceptionTests, Create) {
		EXPECT_THROW(COEUS_THROW(Exception("Some Reason")), ::Coeus::Exception);
	}

	TEST(ExceptionTests, Types) {
		EXPECT_THROW(COEUS_THROW(Exception()), ::Coeus::Exception);
		EXPECT_THROW(COEUS_THROW(Exception()), ::std::exception);
	}

	TEST(ExceptionTests, DefaultData) {
		try {
			COEUS_THROW(Exception());
		} catch(const Exception &e) {
			EXPECT_TRUE(e.has<::Coeus::File>());
			EXPECT_TRUE(e.has<::Coeus::Line>());
			EXPECT_TRUE(e.has<::Coeus::Function>());
#			ifdef USE_STACKTRACE
				EXPECT_TRUE(e.has<::Coeus::Stack>());
#			endif
		}
	}

	TEST(ExceptionTests, Data) {
		try {
			COEUS_THROW(Exception()
				<< TestInt(10)
				<< TestString("This is a string"s)
			);
		} catch(const ::Coeus::Exception &e) {
			ASSERT_TRUE(e.has<TestInt>());
			EXPECT_EQ(*e.get<TestInt>(), 10);
			ASSERT_TRUE(e.has<TestString>());
			EXPECT_EQ(*e.get<TestString>(), "This is a string"s);
		}
	}

	TEST(ExceptionTests, Output) {
		try {
			COEUS_THROW(Exception());
		} catch(const Exception &e) {
			stringstream s;
			s << e;
			EXPECT_NE(s.str().size(), 0);
		}
	}
}
