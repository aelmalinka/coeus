/*	Copyright 2019 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Log.hh"
#include <sstream>

#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/sinks/sync_frontend.hpp>

using namespace testing;
using namespace std;
using namespace Coeus;

namespace {
	class LogTests :
		public ::testing::Test
	{
		public:
			LogTests() :
				s(boost::make_shared<stringstream>())
			{}
		protected:
			void SetUp() override {
				using namespace boost::log;
				using boost::log::sinks::text_ostream_backend;
				using boost::log::sinks::synchronous_sink;

				core::get()->remove_all_sinks();

				auto back = boost::make_shared<text_ostream_backend>();

				back->add_stream(s);
				back->auto_flush(true);

				auto sink = boost::make_shared<
					synchronous_sink<text_ostream_backend>
				>(back);

				core::get()->add_sink(sink);
			}
			boost::shared_ptr<stringstream> s;
	};

	TEST_F(LogTests, TrivalLog) {
		Log::Source log;

		COEUS_LOG(log, Severity::Info) << "This is a test";

		EXPECT_NE(s->str().size(), 0);
		EXPECT_NE(s->str().find("This is a test"s), string::npos);
	}
}
