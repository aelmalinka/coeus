/*	Copyright 2017 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/SharedData.hh"

using namespace std;
using namespace testing;
using namespace Coeus;

namespace {
	namespace detail {
			struct shared_data
			{
				shared_data();
				explicit shared_data(const int);
				int Data;
			};
	}

	using Unique = SharedData<detail::shared_data>;
	using Shared = SharedData<detail::shared_data, std::shared_ptr<detail::shared_data>>;
	using New = SharedData<detail::shared_data, detail::shared_data *>;

	template<typename T = Unique>
	class TestData :
		private T
	{
		public:
			TestData() :
				T()
			{}
			explicit TestData(const int v) :
				T(v)
			{}
			int &Data() {
				return this->shared()->Data;
			}
			const int &Data() const {
				return this->shared()->Data;
			}
	};

	TEST(SharedData, Basic) {
		{
			TestData<> a;
			TestData<> b;

			TestData<> c = a;
			TestData<> d = b;

			TestData<> e(a);
			TestData<> j = TestData();
			TestData<> f = move(j);

			const TestData<> g;
			const TestData<> h(a);
			const TestData<> i = a;

			EXPECT_EQ(a.Data(), b.Data());
			EXPECT_EQ(a.Data(), c.Data());
			EXPECT_EQ(a.Data(), d.Data());
			EXPECT_EQ(a.Data(), e.Data());
			EXPECT_EQ(a.Data(), f.Data());
			EXPECT_EQ(a.Data(), g.Data());
			EXPECT_EQ(a.Data(), h.Data());
			EXPECT_EQ(a.Data(), i.Data());

			a.Data() = 20;

			EXPECT_EQ(a.Data(), b.Data());
			EXPECT_EQ(a.Data(), c.Data());
			EXPECT_EQ(a.Data(), d.Data());
			EXPECT_EQ(a.Data(), e.Data());
			EXPECT_EQ(a.Data(), f.Data());
			EXPECT_EQ(a.Data(), g.Data());
			EXPECT_EQ(a.Data(), h.Data());
			EXPECT_EQ(a.Data(), i.Data());

			b.Data() = 30;

			EXPECT_EQ(a.Data(), b.Data());
			EXPECT_EQ(a.Data(), c.Data());
			EXPECT_EQ(a.Data(), d.Data());
			EXPECT_EQ(a.Data(), e.Data());
			EXPECT_EQ(a.Data(), f.Data());
			EXPECT_EQ(a.Data(), g.Data());
			EXPECT_EQ(a.Data(), h.Data());
			EXPECT_EQ(a.Data(), i.Data());
		}

		TestData<> a;
		EXPECT_EQ(a.Data(), 10);
	}

	TEST(SharedData, WithConstructor) {
		TestData<> a(20);
		TestData<> b(30);

		EXPECT_EQ(a.Data(), 20);
		EXPECT_EQ(b.Data(), 20);
	}

	TEST(SharedData, BasicShared) {
		{
			TestData<Shared> a;
			TestData<Shared> b;

			TestData<Shared> c = a;
			TestData<Shared> d = b;

			TestData<Shared> e(a);
			TestData<Shared> j = TestData<Shared>();
			TestData<Shared> f = move(j);

			const TestData<Shared> g;
			const TestData<Shared> h(a);
			const TestData<Shared> i = a;

			EXPECT_EQ(a.Data(), b.Data());
			EXPECT_EQ(a.Data(), c.Data());
			EXPECT_EQ(a.Data(), d.Data());
			EXPECT_EQ(a.Data(), e.Data());
			EXPECT_EQ(a.Data(), f.Data());
			EXPECT_EQ(a.Data(), g.Data());
			EXPECT_EQ(a.Data(), h.Data());
			EXPECT_EQ(a.Data(), i.Data());

			a.Data() = 20;

			EXPECT_EQ(a.Data(), b.Data());
			EXPECT_EQ(a.Data(), c.Data());
			EXPECT_EQ(a.Data(), d.Data());
			EXPECT_EQ(a.Data(), e.Data());
			EXPECT_EQ(a.Data(), f.Data());
			EXPECT_EQ(a.Data(), g.Data());
			EXPECT_EQ(a.Data(), h.Data());
			EXPECT_EQ(a.Data(), i.Data());

			b.Data() = 30;

			EXPECT_EQ(a.Data(), b.Data());
			EXPECT_EQ(a.Data(), c.Data());
			EXPECT_EQ(a.Data(), d.Data());
			EXPECT_EQ(a.Data(), e.Data());
			EXPECT_EQ(a.Data(), f.Data());
			EXPECT_EQ(a.Data(), g.Data());
			EXPECT_EQ(a.Data(), h.Data());
			EXPECT_EQ(a.Data(), i.Data());
		}

		TestData<Shared> a;
		EXPECT_EQ(a.Data(), 10);
	}

	TEST(SharedData, WithConstructorShared) {
		TestData<Shared> a(20);
		TestData<Shared> b(30);

		EXPECT_EQ(a.Data(), 20);
		EXPECT_EQ(b.Data(), 20);
	}

	TEST(SharedData, BasicNew) {
		{
			TestData<New> a;
			TestData<New> b;

			TestData<New> c = a;
			TestData<New> d = b;

			TestData<New> e(a);
			TestData<New> j = TestData<New>();
			TestData<New> f = move(j);

			const TestData<New> g;
			const TestData<New> h(a);
			const TestData<New> i = a;

			EXPECT_EQ(a.Data(), b.Data());
			EXPECT_EQ(a.Data(), c.Data());
			EXPECT_EQ(a.Data(), d.Data());
			EXPECT_EQ(a.Data(), e.Data());
			EXPECT_EQ(a.Data(), f.Data());
			EXPECT_EQ(a.Data(), g.Data());
			EXPECT_EQ(a.Data(), h.Data());
			EXPECT_EQ(a.Data(), i.Data());

			a.Data() = 20;

			EXPECT_EQ(a.Data(), b.Data());
			EXPECT_EQ(a.Data(), c.Data());
			EXPECT_EQ(a.Data(), d.Data());
			EXPECT_EQ(a.Data(), e.Data());
			EXPECT_EQ(a.Data(), f.Data());
			EXPECT_EQ(a.Data(), g.Data());
			EXPECT_EQ(a.Data(), h.Data());
			EXPECT_EQ(a.Data(), i.Data());

			b.Data() = 30;

			EXPECT_EQ(a.Data(), b.Data());
			EXPECT_EQ(a.Data(), c.Data());
			EXPECT_EQ(a.Data(), d.Data());
			EXPECT_EQ(a.Data(), e.Data());
			EXPECT_EQ(a.Data(), f.Data());
			EXPECT_EQ(a.Data(), g.Data());
			EXPECT_EQ(a.Data(), h.Data());
			EXPECT_EQ(a.Data(), i.Data());
		}

		TestData<New> a;
		EXPECT_EQ(a.Data(), 10);
	}

	TEST(SharedData, WithConstructorNew) {
		TestData<New> a(20);
		TestData<New> b(30);

		EXPECT_EQ(a.Data(), 20);
		EXPECT_EQ(b.Data(), 20);
	}

	detail::shared_data::shared_data()
		: Data(10)
	{}

	detail::shared_data::shared_data(const int val)
		: Data(val)
	{}
}
