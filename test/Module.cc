/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include "Module.hh"

using namespace std;
using namespace Coeus::Tests;

namespace {
	class ModuleImpl :
		public Module
	{
		public:
			ModuleImpl() :
				_thing(false),
				_other(false)
			{}
			void doThing() {
				_thing = true;
			}
			void doOther() {
				_other = true;
			}
			bool isThing() const {
				return _thing;
			}
			bool isOther() const {
				return _other;
			}
		private:
			bool _thing;
			bool _other;
	};
}

COEUS_MODULE(ModuleImpl)
