/*	Copyright 2020 (c) Michael Thomas (malinka) <malinka@entropy-development.com>
	Distributed under the terms of the GNU Affero General Public License v3
*/

#include <gtest/gtest.h>
#include "../src/Event.hh"

using namespace std;
using namespace testing;
using namespace Coeus;

namespace {
	struct Event1 :
		public Event
	{
		static id_type const ID;
	};

	struct Event2 :
		public Event
	{
		static id_type const ID;
	};

	Event::id_type const Event1::ID = type_index(typeid(Event1));
	Event::id_type const Event2::ID = type_index(typeid(Event2));

	TEST(EventTests, HasSameId) {
		{
			Event1 a;
			Event1 b;

			EXPECT_EQ(a.Id(), Event1::ID);
			EXPECT_EQ(b.Id(), Event1::ID);
			EXPECT_EQ(a.Id(), b.Id());
		}

		{
			Event2 a;
			Event2 b;

			EXPECT_EQ(a.Id(), Event2::ID);
			EXPECT_EQ(b.Id(), Event2::ID);
			EXPECT_EQ(a.Id(), b.Id());
		}
	}

	TEST(EventTests, HasDifferentId) {
		Event1 a;
		Event2 b;

		EXPECT_EQ(a.Id(), Event1::ID);
		EXPECT_EQ(b.Id(), Event2::ID);
		EXPECT_NE(a.Id(), b.Id());
	}

	TEST(EventTests, ReferenceHasSameId) {
		{
			Event1 _a, _b;
			Event &a = _a;
			Event &b = _b;

			EXPECT_EQ(a.Id(), Event1::ID);
			EXPECT_EQ(b.Id(), Event1::ID);
			EXPECT_EQ(a.Id(), b.Id());
		}

		{
			Event1 _a, b;
			Event &a = _a;

			EXPECT_EQ(a.Id(), Event1::ID);
			EXPECT_EQ(b.Id(), Event1::ID);
			EXPECT_EQ(a.Id(), b.Id());
		}

		{
			Event2 _a, _b;
			Event &a = _a;
			Event &b = _b;

			EXPECT_EQ(a.Id(), Event2::ID);
			EXPECT_EQ(b.Id(), Event2::ID);
			EXPECT_EQ(a.Id(), b.Id());
		}

		{
			Event2 _a, b;
			Event &a = _a;

			EXPECT_EQ(a.Id(), Event2::ID);
			EXPECT_EQ(b.Id(), Event2::ID);
			EXPECT_EQ(a.Id(), b.Id());
		}
	}

	TEST(EventTests, ReferenceHasDifferentId) {
		Event1 _a;
		Event2 _b;
		Event &a = _a;
		Event &b = _b;

		EXPECT_EQ(a.Id(), Event1::ID);
		EXPECT_EQ(b.Id(), Event2::ID);
		EXPECT_NE(a.Id(), b.Id());
	}

	TEST(EventTests, PointerHasSameId) {
		{
			unique_ptr<Event> a = make_unique<Event1>();
			unique_ptr<Event> b = make_unique<Event1>();

			EXPECT_EQ(a->Id(), Event1::ID);
			EXPECT_EQ(b->Id(), Event1::ID);
			EXPECT_EQ(a->Id(), b->Id());
		}

		{
			unique_ptr<Event> a = make_unique<Event1>();
			Event1 b;

			EXPECT_EQ(a->Id(), Event1::ID);
			EXPECT_EQ(b.Id(), Event1::ID);
			EXPECT_EQ(a->Id(), b.Id());
		}

		{
			unique_ptr<Event> a = make_unique<Event2>();
			unique_ptr<Event> b = make_unique<Event2>();

			EXPECT_EQ(a->Id(), Event2::ID);
			EXPECT_EQ(b->Id(), Event2::ID);
			EXPECT_EQ(a->Id(), b->Id());
		}

		{
			unique_ptr<Event> a = make_unique<Event2>();
			Event2 b;

			EXPECT_EQ(a->Id(), Event2::ID);
			EXPECT_EQ(b.Id(), Event2::ID);
			EXPECT_EQ(a->Id(), b.Id());
		}
	}

	TEST(EventTests, PointerHasDifferentId) {
		unique_ptr<Event> a = make_unique<Event1>();
		unique_ptr<Event> b = make_unique<Event2>();

		EXPECT_EQ(a->Id(), Event1::ID);
		EXPECT_EQ(b->Id(), Event2::ID);
		EXPECT_NE(a->Id(), b->Id());
	}
}
